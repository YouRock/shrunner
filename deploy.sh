#!/bin/bash

sudo chmod 755 deploy.sh # for service

mode=""
url_root=""
port=""
work_dir=""

# get provided flags
while getopts m:u:p:d: option
do
 case "${option}"
 in
 m) mode=${OPTARG};;
 u) url_root=${OPTARG};;
 p) port=${OPTARG};;
 d) work_dir=${OPTARG};;
 esac
done

cd $work_dir


# if you faced problems, check it out https://askubuntu.com/questions/304999/not-able-to-execute-a-sh-file-bin-bashm-bad-interpreter

# flags can be empty, this is the check on it
if [ -z "$mode" ]; then
    # echo "Provide mode argument, please. Mode can be 'LOCAL', 'DEV', 'RELEASE', 'LOCAL_WEB"
    mode="LOCAL_WEB"
    exit
fi

# flags can be empty, this is the check on it
if [ -z "$url_root" ]; then
    # echo "Provide url_root argument, please. (5.164.194.166 for example)"
    url_root="localhost"
    exit
fi

# flags can be empty, this is the check on it
if [ -z "$port" ]; then
    # echo "Provide port argument, please. (5000 for example)"
    port="5000"
    exit
fi

# delete previous env variables from the environment
sed -e '/export MODE/d' -i venv/bin/activate
sed -e '/export BASE_URL/d' -i venv/bin/activate
sed -e '/export URL_ROOT/d' -i venv/bin/activate
sed -e '/export PORT/d' -i venv/bin/activate

# add provided env variables to the environment
echo "export MODE=$mode" >> venv/bin/activate
echo "export BASE_URL=http://$url_root:$port" >> venv/bin/activate
echo "export URL_ROOT=$url_root" >> venv/bin/activate
echo "export PORT=$port" >> venv/bin/activate

sudo ufw allow ${port}

source venv/bin/activate
uwsgi --workers 1 --threads 4 --socket $url_root:$port --protocol=http -w main --pyargv $mode --pyargv $url_root --pyargv $port
