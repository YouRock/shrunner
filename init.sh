#!/bin/bash

# if you faced problems, check it out https://askubuntu.com/questions/304999/not-able-to-execute-a-sh-file-bin-bashm-bad-interpreter

cd $1

# create environment
sudo apt-get update -y
sudo apt-get install python3
sudo apt-get install python3-pip
sudo apt-get install python3-venv

sudo python3 -m venv venv
source venv/bin/activate
# install dependencies
sudo pip3 install wheel
sudo pip3 install uwsgi
sudo pip3 install -r requirements.txt
