#!/bin/bash

mode=""
url_root=""
port=""
work_dir=""

# get provided flags
while getopts m:u:p:d:n: option
do
 case "${option}"
 in
 m) mode=${OPTARG};;
 u) url_root=${OPTARG};;
 p) port=${OPTARG};;
 d) work_dir=${OPTARG};;
 n) proj_name=${OPTARG};;
 esac
done

# flags can be empty, this is the check on it
if [ -z "$mode" ]; then
    # echo "Provide mode argument, please. Mode can be 'LOCAL', 'DEV', 'RELEASE', 'LOCAL_WEB"
    mode="LOCAL_WEB"
fi

# flags can be empty, this is the check on it
if [ -z "$url_root" ]; then
    echo "Provide url_root argument, please. (5.164.194.166 for example)"
    url_root="localhost"
    exit
fi

# flags can be empty, this is the check on it
if [ -z "$port" ]; then
    echo "Provide port argument, please. (5000 for example)"
    port="5000"
    exit
fi

sudo chmod 755 init.sh
sudo chmod 755 deploy.sh


# sudo ./init.sh $work_dir

echo "AAAA"

rec_path=/code/shrunner

echo "BBBB"

path_to_run="${rec_path}/${proj_name}_run.sh"

echo "!!!!!"
sudo touch ${proj_name}_run.sh
sudo chmod 755 ${proj_name}_run.sh
sudo cat /dev/null > ${proj_name}_run.sh
sudo echo "#!/bin/bash" >> ${proj_name}_run.sh
sudo echo cd ${rec_path} >> ${proj_name}_run.sh
sudo echo sudo ./deploy.sh -m $mode -u $url_root -p $port -d $work_dir >> ${proj_name}_run.sh

echo "?????"

service_path=/etc/systemd/system/${proj_name}.service

sudo touch $service_path
sudo cat template.service > $service_path

sudo sed -i "s@{work_dir}@${work_dir}@g" "$service_path"
sudo sed -i "s@{path_to_run}@${path_to_run}@g" "$service_path"

sudo systemctl start ${proj_name}.service
sudo systemctl enable ${proj_name}.service
sudo systemctl status ${proj_name}.service
